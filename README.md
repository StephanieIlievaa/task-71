# A React Task
In this task we will create a **Terms and Conditions** acceptor. The main purpose of the app is to make sure the use has read the whole document.
## Objective
* Checkout the dev branch.
* Create a **Document** compoment, which will contain the rendered content of the Term and Conditions.
* Render the new component in the **App** component.
* In th **App** component, we make a request to the Lorem Ipsum API using **fetch** and get a random long text.
* Create a button, the button should enable itself.
* When implemented, merge the dev branch into master.
## Requirements
* The project starts with **npm run start**.
* The **Document** component must accept **title** and **content** props.
* The **title** should have value **"Terms and Conditions"** and must be placed inside an element with a **.title** class.
* The request must be in the **App** component and pass the fetched text result as a prop to the **Document** component.
* Upon receiving the response from the request, we must use **response.text()**
* The fetched content must be put inside an element with a **.content**  class.
* The **.content** container must be scrollable and the text inside it must overflow.
* The button does not have requirements for its classes but it should be an HTML button. Don't make anything custom.
* The button must have the text of **"I Agree"**.
## Gotchas
**Lorem Ipsum API** - https://jaspervdj.be/lorem-markdownum/markdown.txt