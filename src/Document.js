import { useState } from "react";

function Document(props) {
  const [isRead, setRead] = useState(false);

  const scrollDown = (e) => {
    const element = e.target
    if (element.scrollHeight + element.scrollTop >= element.scrollHeight) {
      setRead(true)

    }
  }
  // should have dynamic content in the Document component???
  return (
    <div className="terms">
      <h1 className="title">{props.title}</h1>
      <p className="content" onScroll={(e) => scrollDown(e)}>{props.content}</p>;
      <button disabled={isRead ? false : true}>I Agree</button>
    </div>
  );
}

export default Document;