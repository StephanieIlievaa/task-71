import "./App.css";
import { useEffect, useState } from 'react';
import Document from './Document.js'

function App() {
  const title = 'Terms and Conditions'
  const [data, setData] = useState('')

  useEffect(() => {
    const url = "https://jaspervdj.be/lorem-markdownum/markdown.txt";

    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const data = await response.text();
        setData(data) 
      } catch (error) {
        console.log("error", error);
      }
    }

    fetchData();
}, []);
  return (
    
       <Document title={title} content={data}></Document>
    
  )
}

export default App;
